import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  View,
  FlatList,
  ActivityIndicator,
  Alert,
  ScrollView,
  Text,
} from 'react-native';
import * as Statics from './statics';
import Icon from 'react-native-vector-icons/FontAwesome';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' +
    'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});
console.disableYellowBox = true;
var vakifData = require('./vakif.json');
import TenantCard from './tenantCard';
import Data from './data';
type Props = {};
export default class App extends Component<Props> {
  renderTenantCard(item, index) {
    return (
      <TenantCard
        isRetrievingAll={ false}
        data={ item }
      />
    );
  }
  render() {
    return (
      <View style={ styles.container }>
          <View style={ styles.header }>
          <View style={ styles.leftHeader }></View>
          <View style={ styles.middleHeader }>
            <Text style={ styles.headerTitle }>{vakifData[0].PYName}</Text>
          </View>

          <View style={ styles.rightHeader }>
            <Icon name="refresh" size={ Statics.size(20) } color="#63c9fc" />
          </View>
          
          </View>
          <FlatList
            style={ styles.dataList }
            keyExtractor={ item => item.id }
            data={ vakifData.slice() }
            renderItem={ ({ item, index }) => (
              this.renderTenantCard( item, index)
            ) }
            ListHeaderComponent={ (
                <View
                  style={ styles.dataPageContainer }
                >
                  <Data
                    transactionData={ vakifData }
                    isReady={ true }
                  />
                </View>
            )}
          />
        </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#dfe6e9',
  },
  dataList: {
    marginTop: Statics.size(5),
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  dataPageContainer: {
    width: Statics.WIDTH,
  },
  header: {
    height: Statics.size(70),
    width: Statics.WIDTH,
    backgroundColor: 'white',
    flexDirection: 'row',
  },
  leftHeader: {
    justifyContent: 'flex-end',
    flex: 1,
    marginBottom: 15,
    
  },
  middleHeader: {
    justifyContent: 'flex-end',
    alignItems: 'center',
    flex: 4,
    marginBottom: 15,
  },
  rightHeader: {
    flex: 1,
    marginBottom: 15,
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  headerTitle: {
    color: '#63c9fc',
    fontSize: 18,
    fontWeight: 'bold',
  }

});
