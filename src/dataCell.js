import React from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet,
  Text,
  View,
  ActivityIndicator,
} from 'react-native';

import * as Statics from './statics';

const Title = ({ title, size }) => (
    <Text style={ [styles.title, { fontSize: size }] }>
      { title }
    </Text>
  );
const DataCell = ({
  title,
  data,
  isReady,
  isLast,
  type,
}) => {
  const containerStyle = {
    borderRightWidth: isLast ? 0 : 1,
  };

  return (
    <View style={ [styles.dataCell, containerStyle] }>
    {type && 
    <View style={ styles.typeContainer }>
        <Text style={{marginBottom: 5,fontSize: 13}}>{type}</Text>
    </View>
    }
    
      <Title size={ Statics.size(14) } title={ title } />
      <View style={ styles.dataCellDataContainer }>
        {
          isReady ?
            <Text style={ styles.dataCellText }>
              { data }
            </Text>
            :
            <ActivityIndicator />
        }
      </View>
    </View>
  );
};
DataCell.propTypes = {
  title: PropTypes.string.isRequired,
  data: PropTypes.string.isRequired,
  isReady: PropTypes.bool.isRequired,
  isLast: PropTypes.bool,
};
DataCell.defaultProps = {
  isLast: false,
};

const styles = StyleSheet.create({
  dataCell: {
    alignItems: 'center',
    borderColor: '#d1d4d7',
    justifyContent: 'center',
    flex: 1,
    marginVertical: Statics.size(0),
  },
  dataCellDataContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    margin: Statics.size(10),
  },
  dataCellText: {
    textAlign: 'center',
    fontSize: 12,
  },
  title: {
    marginVertical: Statics.size(2),
    fontWeight: '500',
    textAlign: 'center',
    alignSelf: 'stretch',
    justifyContent: 'center',
    alignItems: 'center',
  },
  typeContainer: {
    borderBottomWidth: 1,
    marginTop:10,
    width: '80%',
    borderBottomColor: 'rgb(220,219,220)',
    marginBottom: 10,
    justifyContent: 'center',
    alignItems :'center',
  },

});
export default DataCell;
