import React from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet,
  Text,
  View,
} from 'react-native';

import * as Statics from './statics';
import DataCell from './dataCell';

const Data = ({ transactionData, isReady }) => {
  let besGerceklesen = 0;
  let besHedeflenen = 0;
  let yillikHayatGerceklesen = 0;
  let yillikHayatHedeflenen = 0;
  
  transactionData.forEach(item => {
      if(item.left.Urun === 'BES') {
          besGerceklesen += parseInt(item.left.Aylik_Gerceklesen);
          besHedeflenen += parseInt(item.left.Aylik_Hedef);
          
      }
      if(item.right.Urun === "Yıllık Hayat") {
          yillikHayatGerceklesen += parseInt(item.right.Aylik_Gerceklesen);
          yillikHayatHedeflenen += parseInt(item.right.Aylik_Hedef);
          
        
      }
    console.log('item', item);
    console.log('besGerceklesen', besGerceklesen);
    console.log('besHedeflenen', besHedeflenen);
    console.log('yillikHayatGerceklesen', yillikHayatGerceklesen);
    console.log('yillikHayatHedeflenen', yillikHayatHedeflenen);
    
    
  });
  console.log('transactionData', transactionData);
  const dailyOrderCount = transactionData.daily_order_count;
  const monthlyOrderCount = transactionData.monthly_order_count;
  const totalOrderCount = transactionData.total_order_count;
  const dailyTurnover = parseInt(transactionData.daily_turnover, 0).toLocaleString('TR'); // .replace(/\./g, ',');
  const monthlyTurnover = parseInt(transactionData.monthly_turnover, 0).toLocaleString('TR');
  const totalTurnover = parseInt(transactionData.total_turnover, 0).toLocaleString('TR');
  const marketplaceName = transactionData.marketplace_name;
  const marketplaceColor = transactionData.marketplace_color;
  return (
    <View style={ styles.dataContainer }>
      {
        <View>
          <View style={ [styles.dataHeaderRow, { backgroundColor: '#63c9fc' }] }>
            <Text style={ styles.dataHeaderText }>
              Genel
            </Text>
          </View>
          <View style={ styles.dataRow }>
            <DataCell
              title="Toplam"
              data={ `${besGerceklesen}` }
              isReady={ isReady }
              type={"BES"}
            />
            <DataCell
              title="Toplam"
              data={ `${yillikHayatGerceklesen}` }
              isReady={ isReady }
              type={"YILLIK HAYAT"}
            />
            <DataCell
              isLast
              title="Tarih Aralığı"
              data={ `1-31 Mart` }
              isReady={ isReady }
            />
          </View>
          <View style={ styles.dataRow }>
            <DataCell
              title="Hedeflenen"
              data={ `${besHedeflenen}` }
              isReady={ isReady }
            />
            <DataCell
              title="Hedeflenen"
              data={ `${yillikHayatHedeflenen}` }
              isReady={ isReady }
            />
            <DataCell
              isLast
              title="Kalan Süre"
              data={ `24 Gün` }
              isReady={ isReady }
            />
          </View>
        </View>
      }
    </View>
  );
};
Data.propTypes = {
  transactionData: PropTypes.object.isRequired,
  isReady: PropTypes.bool.isRequired,
};
const styles = StyleSheet.create({
  dataRow: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  dataContainer: {
    borderWidth: Statics.size(1),
    borderColor: '#d1d4d7',
    marginVertical: Statics.size(10),
    marginHorizontal: Statics.size(10),
    alignSelf: 'stretch',
    backgroundColor: 'white',
  },
  dataHeaderRow: {
    paddingVertical: 8,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'stretch',
  },
  dataHeaderText: {
    fontWeight: 'bold',
    color: 'white',
    fontSize: 16,
  },
});
export default Data;
