import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  ActivityIndicator,
  Alert,
} from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';
import * as Statics from './statics';


export default class TenantCard extends Component {
  static propTypes = {
    isRetrievingAll: PropTypes.bool.isRequired,
    data: PropTypes.array.isRequired,
  }

  constructor(props) {
    super(props);
    this.state = {
      showLoading: false,
    };
  }

  renderLoading(showLoading) {
    if (showLoading) {
      return <ActivityIndicator color="white" />;
    } else {
      return <Icon name="refresh" size={ Statics.size(20) } color="white" />;
    }
  }
  render() {
    const { showLoading } = this.state;
    const data = this.props.data;
    console.log('data', data)
    return (
      <View style={ styles.container }>
        <TouchableOpacity activeOpacity={9} style={ [styles.headerContainer, { backgroundColor: data.color }] }>
          <Text style={ styles.headerText }>
            {data.BranchName}
          </Text>
          <View style={ styles.refreshContainer }>
            { this.renderLoading(showLoading || this.props.isRetrievingAll) }
          </View>
        </TouchableOpacity>
        <View style={ styles.infoContainer }>
          <View style={ styles.infoSettlementContainer } >
            <View style={ styles.infoSettlementTitleContainer }>
              <Text style={ styles.infoSettlementTitleText }>BES</Text>
            </View>
            <View style={ styles.infoSettlementDataContainer }>
              <View style={ styles.subContainer }>
                <Text style={ styles.infoBigText }>
                  {data.left.Aylik_Gerceklesen}
                </Text>
                <Text style={ styles.infoSmallText }>
                  { 'GERÇEKLEŞEN' }
                </Text>
              </View>
              <View style={ styles.subContainer }>
                <Text style={ styles.infoBigText }>
                  {data.left.Aylik_Hedef}
                </Text>
                <Text style={ styles.infoSmallText }>
                  { 'HEDEFLENEN' }
                </Text>
              </View>
            </View>
          </View>
          <View style={ styles.infoSettlementContainer } >
            <View style={ styles.infoSettlementTitleContainer }>
              <Text style={ styles.infoSettlementTitleText }>YILLIK HAYAT</Text>
            </View>
            <View style={ styles.infoSettlementDataContainer }>
              <View style={ styles.subContainer }>
                <Text style={ styles.infoBigText }>
                  {data.right.Aylik_Gerceklesen}
                </Text>
                <Text style={ styles.infoSmallText }>
                  { 'GERÇEKLEŞEN' }
                </Text>
              </View>
              <View style={ styles.subContainer }>
                <Text style={ styles.infoBigText }>
                  {data.right.Aylik_Hedef}
                </Text>
                <Text style={ styles.infoSmallText }>
                  { 'HEDEFLENEN' }
                </Text>
              </View>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginBottom: Statics.size(10),
    marginHorizontal: Statics.size(10),
    backgroundColor: 'white',
    borderColor: '#d1d4d7',
    borderWidth: Statics.size(1),
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: Statics.size(10),
  },
  headerText: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 16,
    marginRight: Statics.size(10),
  },
  infoContainer: {
    paddingVertical: Statics.size(7),
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  subContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 10,
    paddingRight: 10,
  },
  infoBigText: {
    color: '#2a2c36',
    fontSize: 12,
    fontWeight: 'bold',
  },
  infoSmallText: {
    fontSize: 8,
    color: 'rgb(102, 106, 109)',
    marginVertical: Statics.size(5),
  },
  infoSettlementTitleText: {
    fontSize: 10,
    color: 'rgb(102, 106, 109)',
    marginBottom: Statics.size(5),
  },
  infoSettlementTitleContainer: {
    width: '75%',
    borderBottomWidth: 1,
    borderColor: '#e0e0e0',
    justifyContent: 'center',
    alignItems: 'center',
  },
  refreshContainer: {
    position: 'absolute',
    top: Statics.size(10),
    right: Statics.size(10),
  },
  infoDateContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  infoSettlementContainer: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  infoSettlementDataContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: Statics.size(5),
  },
});
